\chapter{Literární rešerše}

Cílem této části bude analýza životního cyklu projektu, analýza a porovnání existujících konkurenčních aplikací a analýza a výběr technologií vhodných pro implementaci aplikace.


\section{Životní cyklus a fáze projektu}

\subsection{Projekt}

Jelikož zásadním pojmem v tématu mé práce je \uv{projekt}, definuji zde, co je v této práci pod tímto pojmem myšleno.

Dle nejstaršího standardu pro projektové řízení PMBOK~\cite{PMBOKStandard} se pojmem projekt rozumí úsilí, které je dočasné a má vést k vytvoření jedinečného produktu, služby nebo výsledku.
V této definici jsou 2 důležité aspekty odlišující projekt od procesu.

\emph{Dočasnost}, která říká, že projekt má mít definovaný začátek a konec.
Nemusí to ale nutně znamenat, že by projekt měl být jakýmkoliv způsobem krátkodobý.
Navíc, výsledky projektu mohou existovat i po ukončení projektu.
Například projekt postavení monumentu vytvoří hodnotu, která může a má za cíl vydržet po staletí.

\emph{Unikátnost}, která říká, že přestože projekt může být opakovatelný, jiné termíny, jiný rozpočet, jiná rozhraní, jiné místo a jiný projektový tým v konečném důsledku vedou k jedinečnosti projektu a produktu jako jeho cíle.\cite{Svozilova2016}
Nejedná se tedy o rutinně opakovanou akci.

Jelikož je tato práce zaměřena na konkrétní využití ve firmě, která se zabývá převážně vývojem softwaru, budu pojmem \uv{projekt} převážně myslet projekt týkající se vývoje softwaru.
Toto ale neznamená, že některé dílčí části nejsou aplikovatelné obecněji.

\subsection{Typy projektů}\label{subsec:typy-projektů}

Projekty lze dělit podle vztahu k podniku a okolí na 2 kategorie popsané v knize Management rizik projektů~\cite{Korecky2011}.

\emph{Externí projekty}, jejichž výsledek je realizován pro externí zákazníky.
Cílem těchto projektů z~pohledu podniku může být dosáhnout co nejvyšší hrubé marže, kdy jsou tyto projekty zdrojem zisku a prostředků pro další rozvoj podniku a také získání referencí pro zákazníky.

\emph{Interní projekty}, jejichž výsledek je využíván interně v podniku.
Cílem může být dosažení konkurenční výhody nebo zefektivnění činnosti a měřítkem úspěchu je dosažení návratnosti vložených prostředků.
Interní projekty můžeme dělit do dalších kategorií.

\emph{Projekty výzkumu a vývoje}, pod což se řadí například i vývoj nového produktu, jehož cílovou skupinou může být externí skupina zákazníků.
Takovýto produkt je ale vyvíjen pro cílovou skupinu a potenciální zájemce, nikoliv pro předem domluvené zákazníky, a tím pádem se povahou řadí spíše k projektům interním.

\emph{Investiční projekty} jsou primárně zaměřeny na pořízení nebo zhodnocení majetku.
Zde může spadat například pořízení strojů či zařízení pro výrobní i nevýrobní aktivity podniku.
Řídit projektově se ale může vyplatit spíše stavbu nového výrobního závodu a podobné případy, kdy má projekt významné dopady.

\emph{Projekty IT}, bez kterých se v dnešní době nedokáže obejít téměř žádná firma.
Tyto projekty jsou často náročné na zadání a výjimečně je možné použít software bez dalších úprav podle potřeb podniku.

\emph{Projekty organizačních změn} nebo restrukturalizace.
Také pro tyto změny platí, že ne vždy naplňují specifika projektu a je vhodné je řídit projektově až od větších rozsahů.
Může jít kromě změn organizace v podnikových procesech například i o předávání části výroby nebo jiných činností externímu podniku (outsourcing) nebo opačným případem převzetí výroby, která byla dříve nakupovaná (insourcing).

\subsection{Vytvoření projektu}

Před samotným zahájením životního cyklu projektu musí být projekt iniciován.
Tato iniciativa může přijít ze strany zákazníka i interního vedení firmy.

\subsection{Životní cyklus a jeho model}\label{subsec:životní-cyklus-a-jeho-model}

Dle normy~\cite{24748-1}, která definuje životní cyklus systému, přičemž pojem \uv{systém} je zde relativně volně definován a teorii systémů můžeme aplikovat na obecnější pojem projektu~\cite{Svozilova2016}, se
životní cykly liší podle povahy, využití a dalších okolností.
Nicméně zde existuje množina základních fází, které jsou charakteristické a od sebe odlišné vzhledem k celému životnímu cyklu jakéhokoliv systému.
Tato množina fází tvoří specifický framework užitečný například pro abstraktnější pohled na stav projektu.
Přechod mezi jednotlivými fázemi poté představuje rozhodovací bod, kdy musí být splněny podmínky umožňující ukončení této fáze.
Těmito podmínkami se zpravidla rozumí dokončení určitých úkolů nebo úspěšné výstupy procesů definovaných v souvisejících normách~\cite{15288} a~\cite{12207}.

\subsubsection{Model životního cyklu}

Model životního cyklu je poté rozdělení projektu na fáze, propojené přechody mezi jednotlivými fázemi (a tedy rozhodnutími viz~\ref{subsec:životní-cyklus-a-jeho-model}).
Tyto fáze jsou propojeny uspořádaně a průchod projektu jednotlivými fázemi modelu představuje posun k dosažení cílů projektu.
Nejdůležitější vlastností takového modelu je možnost ohlídat dokončení procesů a úkolů před přechodem do další fáze.
Druhotnou vlastností je příležitost pro organizaci využít tento model jako součást většího frameworku, což může mít přínosy pro obchodní a strategické řízení celé organizace~\cite{24748-1}.

\begin{figure}
    \centering
    \includegraphics[width=0.7\textwidth]{text/img/lifecycle}
    \caption{Příklad modelu životního cyklu projektu}
    \label{fig:příklad-modelu-životního-cyklu-projektu}
\end{figure}

Tabulku porovnání fází projektu z pohledu různých autorů uvádí Korecký~\cite[obr. 1.19]{Korecky2011} a já si ji dovolím zde přiložit (Obrázek~\ref{fig:porovnání-modelů}) pro dobrou názornost.

\begin{figure}
    \centering
    \includegraphics[width=0.7\textwidth]{text/img/models}
    \caption[Fáze projektu různých autorů pro různá odvětví]{Fáze projektu z pohledu různých autorů a pro různá odvětví~\cite[str. 63]{Korecky2011}}
    \label{fig:porovnání-modelů}
\end{figure}

\subsubsection{Fáze projektu}\label{subsubsec:faze}

Definice a počet konkrétních fází se liší podle typu a rozsahu projektu a přizpůsobuje se potřebám jeho řízení.
Různé metodiky dávají příklady, mezi nimiž dokážeme jednoduše najít podobnost.
Například PMBOK~\cite{PMBOKStandard} dává za příklad proveditelnost, návrh, konstrukci, testování, nasazení a~uzavření.
Norma~\cite{24748-1} poté definuje \uv{běžně se vyskytující} fáze počínaje konceptem, dále vývojem, produkcí, zužitkováním, podporou a konče výslužbou.

Shrnu zde jednotlivé fáze tak, jak je popisuje norma ISO/IEC/IEEE 24748-1:2018~\cite{24748-1}.

Počáteční fází je \emph{koncept}.
V této fázi je hlavním cílem získat základní přehled o požadavcích a~smyslu projektu, analyzovat dostupná fakta a rámcově odhadnout rozsah projektu a jeho proveditelnost.
Součástí může být předběžná analýza rizik.
Tato fáze víceméně odpovídá fázi proveditelnosti a části fáze návrhu podle PMBOK~\cite{PMBOKStandard}.
Typickými výstupy této fáze jsou požadavky zúčastněných stran, zhodnocení proveditelnosti, jednoduché prototypy a odhady potřebných lidských zdrojů a časové náročnosti.

Rozhodnutím je na konci této fáze, zda ukončit další práci na projektu, či začít fázi \emph{vývoje}.

Fáze vývoje začíná upřesněním požadavků definovaných v předchozí fázi.
Tato část fáze odpovídá části fáze návrhu podle PMBOK~\cite{PMBOKStandard}.
Dále zde spadá celá fáze konstrukce.
Produkt, služba nebo jiný cíl projektu je v této fázi navrhnut, zkonstruován, integrován a otestován.
Výstupem této fáze je produkt, služba nebo jiný výsledek projektu, který je řádně ověřen a otestován a~existují plány pro realizaci následující fáze \emph{produkce}.

Rozhodnutím vedoucím k začátku této fáze je povolení k produkci.
Samotnou produkcí může být kromě klasické výroby produktu také například nasazení softwaru a zpřístupnění pro cílovou skupinu.
Produkce může pokračovat po všechny následující fáze životního cyklu projektu.
V~průběhu této fáze může produkt projít změnami a vylepšeními.
Předpokladem je, že organizace má dostatek všech prostředků pro realizaci produkce.

Fáze \emph{zužitkování} začíná po instalaci nebo například uvedení produktu do prodeje.
Tato fáze by měla představovat naplnění cílů projektu.
Produkt by měl sloužit zamýšlenému užitku.
Z~různých důvodů může být potřeba monitorovat a předcházet anomáliím.
Z tohoto důvodů může existovat souběžná fáze \emph{podpory}.

V této fázi může docházet k malým změnám a opravám v rámci údržby.
Do této fáze spadá například poslední fáze upraveného kaskádového modelu životního cyklu vývoje softwaru, tak jak jej upravil W. Royce~\cite{royce1970} již v roce 1970 a je známou součástí teorie softwarového inženýrství.
Přísnější forma tohoto modelu nedovolující jakékoliv iterace je také známá jako \emph{vodopádový model}, ačkoliv není jasné, kdo s tímto pojmem přišel a dle~\cite{Weisert2003} je spíše používán jako ukázka špatného modelu při porovnání s
novějšími modely.

Fáze \emph{výslužby} je poslední fází životního cyklu.
Tato fáze nastává při potřebě ukončení před\-chozích fází, která může být způsobena několika různými důvody.
Norma ISO/IEC/IEEE 24748-1:2018~\cite{24748-1} popisující teorii systémů dává za příklad třeba nahrazení novým systémem, nenapravitelné poškození, kritickou chybu, nevyužívání původním uživatelem nebo ekonomickou nevý\-hodnost údržby a
pokračování projektu.
Tímto může být považován projekt za ukončený, což obecněji popisuji v následující sekci.

V případě projektů pro externího zadavatele lze do životního cyklu propojit fáze obchodního procesu s fází koncepce, respektive fázemi proveditelnosti a návrhu.
Jedná se o fázi přípravy projektu, zpracování nabídky, podání nabídky, a získání kontraktu v případě úspěchu~\cite{Korecky2011}.

\subsection{Konec projektu}

Díky dočasnosti projektu můžeme mluvit o jeho konci.
V~\cite{PMBOKGuide} je vyjmenováno několik typů konce projektu.

Projekt již mohl dosáhnout svých cílů a tudíž může být ukončen, nebo naopak je jasné, že cíle splněny být nemohou a projekt ztratil na smyslu.
Kvůli závislosti na zdrojích může být projekt ukončen jejich vyčerpáním.
Po dobu trvání projektu mohlo dojít ke změně, která způsobila pominutí potřeby dosažení cílů projektu.
Nebo také mohl být projekt zrušen z právních důvodů.

\subsection{Účastníci projektu}\label{subsec:účastníci-projektu}

Tato sekce obsahuje výčet a stručný popis jednotlivých možných rolí na projektu.
Účastníci, někde může být použit termín \emph{zainteresované strany}, což je překlad odpovídajícího anglického termínu \emph{stakeholder}, nemusí být omezeni na jednu roli, naopak jednotlivé role často splývají v~jedné osobě.
Nejčastější případ je splývání role zadavatel a vlastník~\cite{Dolezal2016}.

Jan Doležal jednotlivé role popsal v~\cite{Dolezal2016}.
\emph{Zadavatel} má zájem projekt zrealizovat a docílit požadované změny, užitku nebo přínosu.
\emph{Zákazník} nebo uživatel jsou osoby, které budou pracovat s výsledky a výstupy projektu v provozní fázi.
\emph{Vlastník} projektu je osoba s dostatečnou autoritou k rozhodování o zásadních aspektech projektu.
Tato osoba je zodpovědná vůči organizaci za byznysový přínos projektu.
\emph{Realizátor} nebo dodavatel představuje zájmy zhotovitelů.
Může to být tedy představitel projektového týmu, který bude dále popsán v následující podsekci.
\emph{Investor} projektu představuje vlastníka finančních nebo jiných zdrojů, které jsou do projektu vkládány.
\emph{Dotčené strany} je poté obecná kategorie, do které patří ti, kteří nepatří do žádné z výše uvedených kategorií, ale projekt se jich nějakým způsobem přímo, či nepřímo dotýká.

\subsubsection{Projektový tým}

Skladba projektového týmu se silně liší podle typu projektu a domény, ve které se projekt pohybuje.
Dokonce se skladba týmu liší i v různých přístupech a teoriích o softwarovém vývoji.
Jednotlivé teorie aplikované v praxi navíc tyto role dále upravují, jak ukázal průzkum \emph{A Systematic Approach to the Comparison of Roles
in the Software Development Processes}~\cite{Yilmaz2012}.
Tento článek navíc shrnuje role podle jednotlivých metod softwarového inženýrství a pro účely návrhu aplikace je zde u vybraných metod vyjmenuji.

Jako první typ článek uvádí \emph{tradiční role}.
Jedná se o role \emph{projektový manažer}, \emph{softwarový vývojář}, \emph{vývojář uživatelského rozhraní}, \emph{databázový návrhář}, \emph{softwarový architekt}, \emph{business analytik}, \emph{inženýr pro řízení požadavků} (z
anglického \emph{requirements engineer}, neexistuje přímý překlad), \emph{tester kvality softwaru} a \emph{systémový analytik}.

Role podle systémového inženýrství~\cite{Sheard1996} a také podle normy ISO/IEC 1220~\cite{12207} pro jejich rozsáhlost vynechám.

Jako další článek zmiňuje role v přístupu extrémního programování podle Kenta Becka~\cite{Beck2002}.
Tyto role jsou programátoři, zákazníci, testeři, stopař, kouč, konzultant a manažer.

Schwaber a Beedle~\cite{Schwaber2002} vymezili šest rolí pro účastníky techniky \emph{Scrum}.
Tyto role jsou scrum master, product owner, zákazník, scrum tým, management, uživatel.

Nejrozsáhlejší popis rolí představuje teorie o \emph{feature-driven development}~\cite{palmer2002practical}, jejíž název lze přeložit jako vývoj řízen užitnými vlastnostmi software~\cite{Buchalcevova2005}.
Účastník projektu zde může mít několik rolí nebo role mohou být sdíleny mezi několika účastníky.
Role se dělí do tří kategorií na klíčové, podporující a doplňkové.
\emph{Klíčové} role jsou projektový manažer, vedoucí softwarový architekt, manažer softwarového vývoje, hlavní programátor, vlastník tříd (class owner) a doménový expert.
Role \emph{podporující} zahrnují manažer vydání (release manager), znalostní expert, inženýr sestavování, specialista na nástroje (toolsmith) a systémový administrátor.
\emph{Doplňkové} role poté jsou testeři, experti na technickou dokumentaci a člověk zodpovědný za nasazení softwaru.


\section{Konkurenční systémy}

V této sekci popíši jednotlivé nástroje nebo kategorie nástrojů či systémů, které již existují a~jsou nějakým způsobem podobné nebo se dají využít pro některou část funkcionality této práce.

\subsection{Komerční systémy}

U všech systémů v této kategorii platí, že nejsou zdarma a k jejich používání je potřeba nějaká forma poplatku.
Toto je zásadní nevýhodou oproti této aplikaci, která je volně dostupná.
Dle zákona č. 111/1998 Sb. o vysokých školách se zveřejňuje celá práce a má aplikace tedy slouží volně k užití podle obdoby licence LGPL, která je popsaná v prohlášení.

\subsection{Nástroje pro plánování projektu}

Celou kategorií nástrojů a systémů jsou nástroje a systémy pro plánování projektu.
Tyto nástroje se často zaměřují na detailní plánování jednotlivých projektů pomocí funkcí jako například vytváření Gantt diagramů.
Příklady těchto nástrojů jsou třeba Milestones Simplicity~\cite{milestones} nebo Microsoft Project~\cite{Aston}.
Cílem této práce není umožnit plánování jednotlivých projektů.
Tyto nástroje tedy pokrývají funkcionalitu této práce jen minimálně a to například možností přehledu o vykázané práci na projektu.

\subsection{Systémy pro celkovou správu projektů}

Další kategorií na trhu jsou systémy pro celkovou správu projektů.
Tyto systémy mají za cíl převzít kontrolu nad samotným řízením projektů a pro organizaci využívající jakýkoliv jiný již zaběhlý a ozkoušený systém,
jsou složitým řešením z důvodu nutnosti migrace na tento nový rozsáhlý systém.

Příkladem v této kategorii je třeba nástroj SpiraTeam společnosti Inflectra \cite{spirateam}.
Tato práce má oproti této kategorii umožňovat nezávislou funkčnost s již existujícími nástroji a systémy.

\subsection{ERP}\label{subsec:erp}

Pro organizace větších rozměrů existují ERP systémy s moduly pro řízení projektů.
Tyto systémy jsou implementovány na míru u jednotlivých organizací a poskytují komplexní řízení a přehled o projektech.
Zvláště pro menší organizace může být nutnost zavedení celého ERP systému neefektivní a zbytečně náročná.
Cílova skupina ERP systémů se tedy liší od cílové skupiny této práce.

\subsubsection{Jira}

Přestože je hlavním zaměřením nástroje Jira správa úkolů, je tento nástroj společnosti Atlassian navrhnut rozšiřitelně a existuje velké množství modulů umožňující přidat do Jira široké spektrum funkcí.

Jedním z modulů umožňujících vytvoření přehledu o projektech v Jira je modul \emph{Jira Insight}.
Tento modul je ale zaměřen spíše obecněji na správu majetku a disponuje konfigurační databází (CMDB) a přestože lze vytvořit přehled projektů a pokrýt tak část požadavků na aplikaci vytvářenou v této práci, některé funkce, jako například
provedení akcí při změně stavu projektu, tento nástroj nepodporuje.


\section{Technologie}\label{sec:technologie}

Obsahem této sekce bude analýza a popis jednotlivých technologií, které jsou vhodné pro vývoj webové aplikace.

Tradičním přístupem jsou statické stránky generované na serveru pro relativně jednoduchého klienta.
Tento přístup má značné nevýhody v rychlosti načítání jednotlivých stránek a uživatel\-ském prožitku používání takovéto webové aplikace~\cite{Mikowski2013//}.

Tyto nedostatky se v minulosti řešily technologiemi jako Java applety nebo Flash.
Tyto přístupy ale měly nevýhody v nutnosti instalace rozšíření do prohlížečů a neefektivním využívání systémových prostředků.

Díky vývoji jazyka Javascript v internetových prohlížečích v posledních letech se otevřela nová možnost vývoje webových aplikací pomocí tzv. SPA v jazyku Javascript.
SPA a jejich historii shrnuje~\cite{Mikowski2013//}.
Tento přístup vývoje aplikace využívá rozhraní prohlížeče pro programování logiky na straně internetového prohlížeče jako klienta.

Pro uživatele má tento přístup několik zásadních výhod.
Tyto aplikace se vykreslují podobným způsobem jako klasická desktop aplikace a uživatelský prožitek je tedy vylepšen odstraněním nutnosti čekání na vygenerování stránky serverem.
Naopak oproti desktop aplikaci je vylepšená přístupnost odstraněním nutnosti instalace takové aplikace.

Dalším nezanedbatelným důvodem pro využití této technologie je oddělení logiky uživatel\-ského prostředí a business logiky aplikace umožňující jednoduchou rozšiřitelnost o využití rozhraní mezi těmito dvěma částmi například mobilní
aplikací.
Naopak nevýhodou může být vzrůsta\-jící složitost implementace celého řešení.

Architektura tohoto přístupu se poté přirozeně dělí na backend a frontend části aplikace.

\subsection{Backend}\label{subsec:backend}

Technologií pro serverovou část webové aplikace existuje v dnešní době nepřeberné množství.
Mezi jednotlivými ekosystémy existují drobné rozdíly, díky kterým se hodí pro určitý typ aplikace.
Nicméně výběr technologie často závisí na zkušenostech a preferencích jednotlivých vývojářů.

Z nejrozšířenějších technologií stojí za zmínku ekosystém okolo jazyka PHP.
PHP se vyvinul z~jazyka určeného pro preprocessing dynamických webových stránek v ekosystém umožňující komplexní tvorbu webových aplikací pomocí frameworků jako jsou například Laravel nebo Symfony.
Často se využívá také ke tvorbě ecommerce řešení ať už jednodušších za pomocí PrestaShop nebo komplexnějších pomocí Adobe Commerce (dříve Magento).
Jednoduchost tohoto ekosystému je ale stále přítomna a občas může být překážkou.

Srovnatelnými možnostmi jsou poté Framework Django v jazyce Python a .NET framework v~jazyce C\#.

Pro jednodušší aplikace, které ale mohou obsluhovat hodně I/O operací je vhodný framework Express.js pro Node.js v jazyce Javascript.
Node.js přinesl do světa serverových aplikací princip event loop pro souběžné zpracování velkého množství požadavků bez nutnosti vytváření vláken pro každý požadavek.

Pro vývoj jsem zvolil ekosystém jazyka Java a frameworku Spring Boot.
Hlavními důvody jsou zkušenost s technologií a vhodnost pro vývoj aplikace tohoto typu.
Relativní novinkou je možnost využití jazyka Kotlin.

\subsubsection{Kotlin}

Kotlin je moderní jazyk od společnosti JetBrains, který vznikl jako plně kompatibilní s Javou a~JVM.
Jedním z jeho cílů je odstranit zásadní nedostatky Javy, které se kvůli stáří této technologie ukázaly a kvůli celkovému návrhu jazyka a zpětné kompatibilitě nemohou být jednoduše odstraněny.
Těmito nedostatky jsou především \emph{null-safety} a upovídanost (anglicky \emph{verbosity}) jazyka.
Inspiraci si Kotlin vzal také z jiných jazyků jako například jazyka Scala.
Tou je například jednotný systém typů, kdy oproti Javě nerozlišuje primitivní a referenční typy~\cite{JohnI.Moore2022}.
Přirozenou volbou pro tvorbu webových aplikací v Kotlinu je framework Ktor, který je ale použitelný spíše pro menší projekty.
Například samotná dependency injection je třeba řešit pomocí externího frameworku Koin.

Pro nevyzrálost tohoto projektu jsem zvolil ověřený framework Spring Boot, který v nedávné době přidal podporu pro Kotlin.

\subsubsection{Spring Boot}

Spring Boot představuje platformu pro vývoj Java aplikací s minimální nutností konfigurace frameworku Spring~\cite{Springbootintro}.
Jednoduchost inicializace frameworku Spring navíc vylepšuje nástroj Spring Initializr, pomocí kterého lze vygenerovat balíček s předdefinovanými technologiemi, knihovnami a závislostmi.

Spring framework obsahuje velké množství knihoven zjednodušujících vývoj jednotlivých částí webové aplikace.
Popíši zde výběr těch nejdůležitějších pro možné využití v aplikaci mého typu.

\subsubsection{Quartz plánovač}

Quartz je knihovna pro plánování a spouštění naplánovaných úloh.
Tato knihovna se nabízí pro použití při pravidelné synchronizaci dat s dalšími systémy.

\subsubsection{Feign klient}

Feign je deklarativní REST klient.
Přináší přehlednou možnost deklarovat, jakým způsobem se aplikace bude integrovat s API dalších systémů.

\subsubsection{Spring WebMVC}

Pro tvorbu webových aplikací komunikujících pomocí protokolu HTTP přináší Spring knihovnu WebMVC, která je postavena na technologii Java Servlet.

Relativně novou možností je knihovna Spring Webflux, která využívá knihovny Project Reactor pro vytváření aplikací zpracovávající HTTP požadavky neblokujícím způsobem a přináší do frameworku Spring princip event loop popularizovaný
frameworkem Node.js.
Tato knihovna přináší výhody především ve škálovatelnosti, které tato aplikace nevyužije, protože je přirozeně určena pro úzkou skupinu uživatelů v jedné organizaci čítající jednotky, maximálně nízké desítky uživatelů~\cite{Chandrakant}.

\subsubsection{Gradle}

Gradle je nástroj pro automatizaci sestavení programu.
Oproti dříve rozšířenějšímu nástroji Maven používajícímu XML, používá doménově specifický jazyk v Groovy nebo Kotlinu.
Gradle je také oficiálním sestavovacím nástrojem pro Android.
Oficiální web Gradle uvádí další výhody oproti nástroji Maven, například flexibilitu a výkon~\cite{gradle}.

\subsection{Databáze}

Díky knihovně Spring Data dokáže aplikace komunikovat s různými databázovými technologiemi.

Pro tento typ webové aplikace se hodí relační databáze z důvodu přítomnosti funkcí jako je ohlídání integrity dat nebo konzistence transakcí.
Těchto databázových systémů dnes existuje velké množství.
Mezi nejznámější databázové systémy patří Microsoft SQL, Oracle Database, MySQL, PostgreSQL a MariaDB.

Z těchto databázových systémů jsou open source pouza dva, a to PostgreSQL a MariaDB.

MariaDB je odnoží MySQL, kterou koupil Sun Microsystems, který byl následně koupen společností Oracle.
Toto se nelíbilo autorovi MySQL Michaelu Wideniusovi, který vytvořil tuto odnož a udal k tomu několik důvodů a obav: \uv{Namísto opravování chyb, Oracle odstraňuje funkce.}~\cite{Pearce2022}

Srovnání těchto dvou open source možností uvádí~\cite{Janik2019}.
Z tohoto srovnání vyplývá, že PostgreSQL se hodí pro složitější úkoly a pro MariaDB (respektive MySQL) mluví jednoduchost, rychlost a bezpečnost zejména pro webové aplikace.

\subsection{Frontend}\label{subsec:frontend}

Vývoj klientských aplikací je omezen, pokud se bavíme o vývoji pro webové prohlížeče, na ekosystém jazyka Javascript.
Jazyk Javascript je implementací standardu ECMAScript společnosti ECMA International.
Pokud pominu možnosti transpilace různých jazyků do jazyka Javascript, jejichž ekosystém a nepřipravenost pro vývoj webového klienta je velkým důvodem proti, existuje v poslední době nejpopulárnější~\cite{2021} možnost rozšíření jazyka
Javascript o statické typování se jménem Typescript.

\subsubsection{Typescript}

Tato nadstavba od společnosti Microsoft se také kompiluje do jazyka Javascript a je primárním jazykem pro vývoj v jednom z nejpopulárnějších Javascript frameworků Angular.
Nejnovější verze frameworku Vue.js, Vue 3, je také napsána pomocí jazyka Typescript a umožňuje jeho použití.

\subsubsection{Vue.js}

V posledních letech tradičně nejpopulárnějšími frameworky pro Javascript a SPA jsou React, Angular a Vue.js~\cite{2021}.
Z těchto 3 je Angular nejrobustnějším frameworkem a má nejvíce plochou křivku učení.
Ve frameworku React je oproti Angularu velká volnost ve výběru knihoven.
Tato volnost znesnadňuje převážně začátek vývoje pro méně zkušené vývojáře.
Nicméně ani tak zásadní část aplikace, jakou je state management nemá v Reactu jasnou volbu ani pro zkušené vývojáře.
K dispozici jsou nejznámější knihovny Redux, MobX a Apollo, nicméně sám Facebook, tvůrce frameworku React, nepoužívá ani jednu z těchto knihoven a použil vlastní implementaci vzoru Flux~\cite{Facebook}.

Framework Vue.js je z této trojice nejmladším frameworkem a snaží se vylepšit a využít nedokonalostí frameworků React a Angular.
Například pro state management má vlastní řešení Vuex a od verze 3 oficiálně doporučuje použití knihovny Pinia~\cite{Rodriguez}.

Pro zjednodušení vývoje uživatelského rozhraní existují knihovny komponent uživatelského rozhraní.
Jednotlivé knihovny shrnuje~\cite{Pattakos} a liší se převážně vzhledem a zaměřením.
PrimeVue je jedna z knihoven zaměřujících se převážně na podnikové aplikace.

\subsection{Výběr technologií}\label{subsec:výběr-technologií}

Na základě analýzy jsem vybral následující technologie.
Pro serverovou část aplikace framework Spring Boot s knihovnami WebMVC, Feign a Quartz, jazyk Kotlin a nástroj pro sestavování Gradle.
Pro databázi MariaDB a pro klientskou část aplikace jazyk Typescript s frameworkem Vue.js a knihovnami Pinia a PrimeVue.
