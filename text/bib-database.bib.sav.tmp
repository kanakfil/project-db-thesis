@Book{PMBOKStandard,
  author    = {{Project Management Institute}},
  date      = {2021-08-01},
  title     = {The standard for project management and A guide to the project management body of knowledge},
  edition   = {Seventh edition},
  isbn      = {9781628256642},
  pagetotal = {250},
  publisher = {Project Management Institute, Inc.},
  subtitle  = {(PMBOK GUIDE)},
  address   = {Newton Square, Pennsylvania},
  ean       = {9781628256642},
  year      = {2021},
}

@Book{PMBOKGuide,
  author    = {{Project Management Institute}},
  title     = {A guide to the project management body of knowledge (PMBOK guide)},
  edition   = {Sixth edition},
  isbn      = {9781628251845},
  language  = {English},
  publisher = {Project Management Institute},
  abstract  = {To support the broadening spectrum of project delivery approaches, PMI is offering A Guide to the Project Management Body of Knowledge (PMBOK® Guide) – Sixth Edition as a bundle with the new Agile Practice Guide. The PMBOK® Guide – Sixth Edition now contains detailed information about agile; while the Agile Practice Guide, created in partnership with Agile Alliance, serves as a bridge to connect waterfall and agile. Together they are a powerful tool for project managers.;New to the Sixth Edition, each knowledge area will contain a section entitled Approaches for Agile, Iterative and Adaptive Environments, describing how these practices integrate in project settings. It will also contain more emphasis on strategic and business knowledge-including discussion of project management business documents-and information on the PMI Talent Triangle™ and the essential skills for success in today's market.;},
  address   = {Newtown Square, Pennsylvania},
  keywords  = {Project management; projektový management},
  year      = {2017},
}

@Article{12207,
  title   = {ISO/IEC/IEEE International Standard - Systems and software engineering -- Software life cycle processes},
  doi     = {10.1109/IEEESTD.2017.8100771},
  pages   = {1-157},
  journal = {ISO/IEC/IEEE 12207:2017(E) First edition 2017-11},
  month   = {11},
  year    = {2017},
}

@ARTICLE{24748-1,  author = {},  journal = {ISO/IEC/IEEE 24748-1:2018(E)},   title = {ISO/IEC/IEEE International Standard - Systems and software engineering - Life cycle management - Part 1:Guidelines for life cycle management},   year = {2018},  volume = {},  number = {},  pages = {1-82},  doi = {10.1109/IEEESTD.2018.8526560} }

@ARTICLE{15288,  author = {},  journal = {ISO/IEC/IEEE 15288 First edition 2015-05-15},   title = {ISO/IEC/IEEE International Standard - Systems and software engineering -- System life cycle processes},   year = {2015},  volume = {},  number = {},  pages = {1-118},  doi = {10.1109/IEEESTD.2015.7106435} }

@book{Svozilova2016,
    author = {Alena Svozilová},
    title = {Projektový management},
    subtitle = {systémový přístup k řízení projektů},
    publisher = {Grada Publishing},
    address = {Praha},
    year = {2016},
    edition = {3., aktualizované a rozšířené vydání},
    isbn = {978-80-271-0075-0},
}

@inproceedings{royce1970,
    added-at = {2015-10-22T22:12:30.000+0200},
    author = {Royce, Winston W.},
    biburl = {https://www.bibsonomy.org/bibtex/2f5b867af83190db129fe8fc46b738dc3/jrdn},
    booktitle = {Proceedings IEEE WESCON},
    interhash = {7f2d1186263afd9c9857967f83ffe579},
    intrahash = {f5b867af83190db129fe8fc46b738dc3},
    keywords = {systems-engineering waterfall},
    month = aug,
    pages = {1-9},
    timestamp = {2015-10-22T22:12:30.000+0200},
    title = {Managing the Development of Large Software Systems},
    year = 1970
}

@online{Weisert2003,
    author = {Conrad Weisert},
    journal = {Information Disciplines, Inc.,},
    title = {There's no such thing as the Waterfall Approach!},
    address = {Chicago},
    year = {2003},
    urldate = {2022-04-22},
    url = {http://www.idinews.com/waterfall.html},
}

@book{Dolezal2016,
    author = {Jan Doležal},
    title = {Projektový management},
    subtitle = {komplexně, prakticky a podle světových standardů},
    publisher = {Grada Publishing},
    address = {Praha},
    year = {2016},
    edition = {První vydání},
    isbn = {978-80-247-5620-2},
}

@book{Korecky2011,
    author = {Michal Korecký and Václav Trkovský},
    title = {Management rizik projektů},
    subtitle = {se zaměřením na projekty v průmyslových podnicích},
    publisher = {Grada},
    address = {Praha},
    year = {2011},
    edition = {1. vyd},
    isbn = {978-80-247-3221-3},
}

@inbook{Yilmaz2012,
    author = {Murat Yilmaz and Rory V. O’Connor and Paul Clarke},
    doi = {10.1007/978-3-642-30439-2_18},
    pages = {198-209},
    title = {A Systematic Approach to the Comparison of Roles in the Software Development Processes},
    year = {2012},
}

@article{Sheard1996,
    author = {Sarah A. Sheard},
    doi = {10.1002/j.2334-5837.1996.tb02100.x},
    issn = {23345837},
    issue = {1},
    journal = {INCOSE International Symposium},
    month = {7},
    pages = {894-902},
    title = {THE VALUE OF TWELVE SYSTEMS ENGINEERING ROLES},
    volume = {6},
    year = {1996},
}

@book{Beck2002,
    author = {Kent Beck},
    title = {Extrémní programování},
    publisher = {Grada},
    address = {Praha},
    year = {2002},
    edition = {1. vyd},
    isbn = {80-247-0300-9},
}

@book{Schwaber2002,
    author = {Ken Schwaber and Mike Beedle},
    title = {Agile software development with Scrum},
    publisher = {Prentice Hall},
    address = {Upper Saddle River},
    year = {2002},
    isbn = {0-13-067634-9},
}

@book{palmer2002practical,
    title = {A Practical Guide to Feature-driven Development},
    author = {Palmer, S.R. and Felsing, J.M.},
    isbn = {9780130676153},
    lccn = {2001055351},
    series = {Coad series},
    url = {https://books.google.cz/books?id=NhlFAAAAYAAJ},
    year = {2002},
    publisher = {Prentice Hall PTR}
}

@article{Buchalcevova2005,
    author = {Alena Buchalcevová},
    title = {Metodika Feature-Driven Development neopouští modelování a procesy, a přesto přináší výhody agilního vývoje},
    address = {Nám. W.Churchilla 4, Praha 3},
    year = {2005},
    urldate = {2022-04-23},
    url = {https://nb.vse.cz/~buchalc/clanky/tsw2005.pdf},
}



@article{Sestakova2018,
    author = {Eliška Šestáková and Jan Janoušek},
    journal = {Information},
    title = {Automata Approach to XML Data Indexing},
    year = {2018},
    volume = {9},
    number = {1},
    ISSN = {2078-2489},
    medium = {online},
    accessed = {2019-03-26},
    DOI = {10.3390/info9010012},
    URL = {http://www.mdpi.com/2078-2489/9/1/12},
}

@book{Crochemore2002,
    author = {Maxime Crochemore and Wojciech Rytter},
    title = {Jewels of stringology},
    publisher = {World Scientific},
    address = {River Edge, NJ},
    year = {2002},
    ISBN = {978-9810247829},
}

@book{Motwani2014,
    author = {Rajeev Motwani and Jeffrey D. Ullman and John E. Hopcroft},
    title = {Introduction to automata theory, languages, and computation},
    publisher = {Pearson},
    address = {Harlow},
    year = {2014},
    edition = {Third},
    ISBN = {9781292039053},
}

@book{Kopka2004,
    author = {Helmut Kopka and Patrick W. Daly},
    title = {LATEX},
    subtitle = {podrobný průvodce},
    publisher = {Computer Press},
    address = {Brno},
    year = {2004},
    ISBN = {80-7226-973-9},
}

@Book{IdentityManagement,
  author    = {Wilson,Yvonne and Hingnikar,Abhishek},
  date      = {2019-12-18},
  title     = {Solving Identity Management in Modern Applications: Demystifying OAuth 2.0, OpenID Connect, and SAML 2.0},
  isbn      = {9781484250952},
  language  = {English},
  pagetotal = {311},
  publisher = {Apress},
  abstract  = {Know how to design and use identity management to protect your application and the data it manages.At a time when security breaches result in increasingly onerous penalties, it is paramount that application developers and owners understand identity management and the value it provides when building applications. This book takes you from account provisioning to authentication to authorization, and covers troubleshooting and common problems to avoid. The authors include predictions about why this will be even more important in the future. Application best practices with coding samples are provided.Solving Identity and Access Management in Modern Applications gives you what you need to design identity and access management for your applications and to describe it to stakeholders with confidence. You will be able to explain account creation, session and access management, account termination, and more.

What You'll Learn

Understand key identity management conceptsIncorporate essential design principlesDesign authentication and access control for a modern applicationKnow the identity management frameworks and protocols used today (OIDC/ OAuth 2.0, SAML 2.0)Review historical failures and know how to avoid them

Who This Book Is For

Developers, enterprise or application architects, business application or product owners, and anyone involved in an application's identity management solution;Know how to design and use identity management to protect your application and the data it manages.At a time when security breaches result in increasingly onerous penalties, it is paramount that application developers and owners understand identity management and the value it provides when building applications. This book takes you from account provisioning to authentication to authorization, and covers troubleshooting and common problems to avoid. The authors include predictions about why this will be even more important in the future. Application best practices with coding samples are provided.Solving Identity and Access Management in Modern Applications gives you what you need to design identity and access management for your applications and to describe it to stakeholders with confidence. You will be able to explain account creation, session and access management, account termination, and more.What You’ll LearnUnderstand key identity management conceptsIncorporate essential design principlesDesign authentication and access control for a modern applicationKnow the identity management frameworks and protocols used today (OIDC/ OAuth 2.0, SAML 2.0)Review historical failures and know how to avoid themWho This Book Is ForDevelopers, enterprise or application architects, business application or product owners, and anyone involved in an application's identity management solution;},
  ean       = {9781484250952},
  year      = {2019},
}

@online{keycloak,
    title = {Keycloak},
    urldate = {2022-04-24},
    url = {https://www.keycloak.org/},
}

@online{GoodKeycloakSpring,
    author = {Michael Good},
    journal = {Baeldung},
    title = {A Quick Guide to Using Keycloak with Spring Boot},
    urldate = {2022-04-24},
    url = {https://www.baeldung.com/spring-boot-keycloak},
}

@Online{crmsystemy,
  author    = {Solitea},
  title     = {TOP 9 trendů v oblasti CRM pro rok 2022},
  url       = {https://crm.cz/cs-cz/top-9-trendu-v-oblasti-crm-pro-rok-2022},
  urldate   = {2022-04-24},
  journal   = {CRM Systémy},
  publisher = {Solitea},
}

@article{Lee2014,
    author = {Young-Chan Lee and Nguyen-Hanh Tang and Vijayan Sugumaran},
    doi = {10.1080/10580530.2013.854020},
    issn = {1058-0530},
    issue = {1},
    journal = {Information Systems Management},
    month = {1},
    pages = {2-20},
    title = {Open Source CRM Software Selection using the Analytic Hierarchy Process},
    volume = {31},
    year = {2014},
}

@Online{raynet,
  author  = {{RAYNET s.r.o.}},
  title   = {Raynet CRM},
  url     = {https://raynet.cz/},
  urldate = {2022-04-24},
  journal = {Raynet},
}

@online{KumarKP1742022,
    author = {Aswin Kumar KP},
    journal = {Disbug},
    title = {Github vs Gitlab vs Bitbucket},
    urldate = {2022-04-24},
    url = {https://disbug.io/en/blog/github-vs-gitlab-vs-bitbucket},
}




@inproceedings{exponential:queries,
    author = {Mandhani, Bhushan and Suciu, Dan},
    title = {Query Caching and View Selection for XML Databases},
    booktitle = {Proceedings of the 31st International Conference on Very Large Data Bases},
    series = {VLDB '05},
    year = {2005},
    isbn = {1-59593-154-6},
    location = {Trondheim, Norway},
    pages = {469--480},
    numpages = {12},
    url = {http://dl.acm.org/citation.cfm?id=1083592.1083648},
    acmid = {1083648},
    publisher = {VLDB Endowment}
}

@inproceedings{dietz1982maintaining,
    title = {Maintaining order in a linked list},
    author = {Dietz, Paul F},
    booktitle = {Proceedings of the fourteenth annual ACM symposium on Theory of computing},
    pages = {122--127},
    year = {1982},
    organization = {ACM}
}

@MISC{ebnf,
    AUTHOR = "{BSI (British Standards Institution)}",
    TITLE = {BS 6154:1981 Method of defining -- syntactic metalanguage},
    YEAR = {1981},
    ISBN = {ISBN 0-580-12530-0},
}

@BOOK{xml:technologie,
    AUTHOR = {Jaroslav Pokorný},
    TITLE = {XML Technologie -- Principy a aplikace v praxi},
    PUBLISHER = {Grada Publishing, a.s.},
    ADDRESS = {Praha},
    YEAR = {2008},
    ISBN = {978-80-247-2725-7},
}


@inproceedings{def:2,
    author = {Leonid Libkin},
    year = {2005},
    isbn = {978-3-540-27580-0},
    booktitle = {Automata, Languages and Programming},
    volume = {3580},
    series = {Lecture Notes in Computer Science},
    editor = {Caires, Luís and Italiano, Giuseppe and Monteiro, Luís and Palamidessi, Catuscia and Yung, Moti},
    title = {Logics for Unranked Trees: An Overview},
    publisher = {Springer Berlin Heidelberg},
    pages = {35--50},
}


@inproceedings{def:1,
    author = {Frank Neven},
    year = {2002},
    isbn = {978-3-540-44240-0},
    booktitle = {Computer Science Logic},
    volume = {2471},
    series = {Lecture Notes in Computer Science},
    editor = {Bradfield, Julian},
    title = {Automata, Logic, and XML},
    publisher = {Springer Berlin Heidelberg},
    pages = {2--26},
}

@Misc{xml:w3c,
  author    = {Bray T. Paoli J. Sperberg-McQueen C. et al},
  title     = {Extensible Markup Language (XML) 1.0},
  url       = {http://www.w3.org/XML},
  urldate   = {2015-02-05},
  publisher = {W3C},
  year      = {Nov 2008},
}

@inproceedings{xml:index:9,
    author = {Chung, Chin-Wan and Min, Jun-Ki and Shim, Kyuseok},
    title = {APEX: An Adaptive Path Index for XML Data},
    booktitle = {Proceedings of the 2002 ACM SIGMOD International Conference on Management of Data},
    series = {SIGMOD '02},
    year = {2002},
    isbn = {1-58113-497-5},
    location = {Madison, Wisconsin},
    pages = {121--132},
    numpages = {12},
    doi = {10.1145/564691.564706},
    acmid = {564706},
    publisher = {ACM},
    address = {New York, NY, USA}
}

@inproceedings{xml:index:8,
    year = {2007},
    isbn = {978-3-540-71702-7},
    booktitle = {Advances in Databases: Concepts, Systems and Applications},
    volume = {4443},
    series = {Lecture Notes in Computer Science},
    editor = {Kotagiri, Ramamohanarao and Krishna, P. Radha and Mohania, Mukesh and Nantajeewarawat, Ekawit},
    title = {AB-Index: An Efficient Adaptive Index for Branching XML Queries},
    url = {http://dx.doi.org/10.1007/978-3-540-71703-4_90},
    publisher = {Springer Berlin Heidelberg},
    author = {Zhang, Bo and Wang, Wei and Wang, Xiaoling and Zhou, Aoying},
    pages = {988--993},
}

@inproceedings{xml:index:10,
    author = {Li, Quanzhong and Moon, Bongki},
    title = {Indexing and Querying XML Data for Regular Path Expressions},
    booktitle = {Proceedings of the 27th International Conference on Very Large Data Bases},
    series = {VLDB '01},
    year = {2001},
    isbn = {1-55860-804-4},
    pages = {361--370},
    numpages = {10},
    url = {http://dl.acm.org/citation.cfm?id=645927.672035},
    acmid = {672035},
    publisher = {Morgan Kaufmann Publishers Inc.},
    address = {San Francisco, CA, USA}
}

@inproceedings{xml:index:7,
    author = {Rao, P. and Moon, B.},
    booktitle = {Data Engineering, 2004. Proceedings. 20th International Conference on},
    title = {PRIX: indexing and querying XML using prufer sequences},
    year = {2004},
    month = {March},
    pages = {288--299},
    keywords = {XML;database indexing;pattern matching;query processing;sequences;tree data structures;trees (mathematics);B-tree;Prufer sequence;XML database;XML document indexing;false alarm;holistic processing;query processing;refinement phase;root-to-leaf path;twig pattern processing;Computer science;Databases;Indexing;Information representation;Internet;Merging;Moon;Pattern matching;Query processing;XML},
    doi = {10.1109/ICDE.2004.1320005},
    ISSN = {1063-6382}
}

@inproceedings{xml:index:11,
    author = {Wang, Haixun and Park, Sanghyun and Fan, Wei and Yu, Philip S.},
    title = {ViST: A Dynamic Index Method for Querying XML Data by Tree Structures},
    booktitle = {Proceedings of the 2003 ACM SIGMOD International Conference on Management of Data},
    series = {SIGMOD '03},
    year = {2003},
    isbn = {1-58113-634-X},
    location = {San Diego, California},
    pages = {110--121},
    numpages = {12},
    doi = {10.1145/872757.872774},
    acmid = {872774},
    publisher = {ACM},
    address = {New York, NY, USA},
}

@inproceedings{xml:index:2,
    author = {Qinghua Zou and Shaorong Liu and Wesley W. Chu},
    title = {Ctree: a compact tree for indexing XML data},
    booktitle = {Web Information and Data Management},
    year = {2004},
    pages = {39--46},
    doi = {10.1145/1031453.1031462},
    masid = {1242245}
}

@inproceedings{xml:index:1,
    author = {Pettovello, P. Mark and Fotouhi, Farshad},
    title = {MTree: An XML XPath Graph Index},
    booktitle = {Proceedings of the 2006 ACM Symposium on Applied Computing},
    series = {SAC '06},
    year = {2006},
    isbn = {1-59593-108-2},
    location = {Dijon, France},
    pages = {474--481},
    numpages = {8},
    doi = {10.1145/1141277.1141389},
    acmid = {1141389},
    publisher = {ACM},
    address = {New York, NY, USA},
    keywords = {XML, XPath, graph, index, threaded paths}
}

@inproceedings{xml:index:5,
    author = {Kaushik, Raghav and Bohannon, Philip and Naughton, Jeffrey F and Korth, Henry F},
    title = {Covering Indexes for Branching Path Queries},
    booktitle = {Proceedings of the 2002 ACM SIGMOD International Conference on Management of Data},
    series = {SIGMOD '02},
    year = {2002},
    isbn = {1-58113-497-5},
    location = {Madison, Wisconsin},
    pages = {133--144},
    numpages = {12},
    doi = {10.1145/564691.564707},
    acmid = {564707},
    publisher = {ACM},
    address = {New York, NY, USA}
}

@inproceedings{xml:index:3,
    author = {Nan Tang and Yu, J.X. and Ozsu, M.T. and Kam-Fai Wong},
    booktitle = {Data Engineering, 2008. On ICDE 2008, IEEE 24th International Conference},
    title = {Hierarchical Indexing Approach to Support XPath Queries},
    year = {2008},
    month = {April},
    pages = {1510--1512},
    keywords = {XML;indexing;query processing;XML path indexing;XPath queries;hierarchical indexing approach;Books;Database languages;Indexing;Robustness;XML},
    doi = {10.1109/ICDE.2008.4497606}
}

@inproceedings{xml:index:6,
    year = {1999},
    isbn = {978-3-540-65452-0},
    booktitle = {Database Theory -- ICDT’99},
    volume = {1540},
    series = {Lecture Notes in Computer Science},
    editor = {Beeri, Catriel and Buneman, Peter},
    title = {Index Structures for Path Expressions},
    url = {http://dx.doi.org/10.1007/3-540-49257-7_18},
    publisher = {Springer Berlin Heidelberg},
    author = {Milo, Tova and Suciu, Dan},
    pages = {277--295},
}


@book{DBLP:books/ox/CrochemoreR94,
    author = {Maxime Crochemore and
 Wojciech Rytter},
    title = {Text Algorithms},
    publisher = {Oxford University Press},
    year = {1994},
    isbn = {0-19-508609-0},
    ee = {http://www-igm.univ-mlv.fr/$\sim$mac/REC/B1.html, http://www.mimuw.edu.pl/$\sim$rytter/BOOKS/text-algorithms.pdf},
    bibsource = {DBLP, http://dblp.uni-trier.de}
}

@book{crochemore2007algorithms,
    title = {Algorithms on strings},
    author = {Crochemore, Maxime and Hancart, Christophe and Lecroq, Thierry},
    year = {2007},
    publisher = {Cambridge Univ Pr}
}


@Misc{xpath:w3c,
  author       = {Clark, J. and DeRose, S.},
  title        = {XML Path Language (XPath) Version 1.0},
  howpublished = {online},
  url          = {http://www.w3.org/TR/xpath},
  citedate     = {2015-02-03},
  publisher    = {W3C},
  year         = {Nov 1999},
}

@Misc{xpointer:w3c,
  author       = {DeRose, S.},
  title        = {XML Pointer Language (XPointer)},
  howpublished = {online},
  url          = {http://www.w3.org/TR/xptr},
  citedate     = {2015-03-04},
  publisher    = {W3C},
  year         = {2002},
}

@Misc{xlink:w3c,
  author       = {DeRose, S.},
  title        = {XML Linking Language (XLink) Version 1.0},
  howpublished = {online},
  url          = {http://www.w3.org/TR/xlink},
  urldate      = {2015-03-04},
  citedate     = {2015-03-04},
  publisher    = {W3C},
  year         = {2001},
}

@MISC{arbology,
    AUTHOR = {Bořivoj Melichar and Jan Janoušek and Tomáš Flouri},
    PUBLISHER = {CTU, Faculty of Information Technology},
    TITLE = {Introduction to Arbology},
    URL = {https://edux.fit.cvut.cz/oppa/PI-ARB/prednasky/arbology.pdf},
    howpublished = {online},
    citedate = {2015/05/01},
}

@inproceedings{papakonstantinou1995object,
    title = {Object exchange across heterogeneous information sources},
    author = {Papakonstantinou, Yannis and Garcia-Molina, Hector and Widom, Jennifer},
    booktitle = {Data Engineering, 1995. Proceedings of the Eleventh International Conference on},
    pages = {251--260},
    year = {1995},
    organization = {IEEE}
}

@article{mchugh1997lore,
    title = {Lore: A database management system for semistructured data},
    author = {McHugh, Jason and Abiteboul, Serge and Goldman, Roy and Quass, Dallan and Widom, Jennifer},
    journal = {SIGMOD record},
    volume = {26},
    number = {3},
    pages = {54--66},
    year = {1997}
}

@techreport{goldman1997dataguides,
    title = {Dataguides: Enabling query formulation and optimization in semistructured databases},
    author = {Goldman, Roy and Widom, Jennifer},
    year = {1997},
    institution = {Stanford}
}

@inproceedings{nestorov1997representative,
    title = {Representative objects: Concise representations of semistructured, hierarchical data},
    author = {Nestorov, Svetlozar and Ullman, Jeffrey and Wiener, Janet and Chawathe, Sudarashan},
    booktitle = {Data Engineering, 1997. Proceedings. 13th International Conference on},
    pages = {79--90},
    year = {1997},
    organization = {IEEE}
}

@article{goldman1999semistructured,
    title = {From semistructured data to XML: Migrating the Lore data model and query language},
    author = {Goldman, Roy and McHugh, Jason and Widom, Jennifer},
    year = {1999}
}

@article{dasg:1,
    title = {Searching subsequences},
    journal = {Theoretical Computer Science},
    volume = {78},
    number = {2},
    pages = {363--376},
    year = {1991},
    issn = {0304-3975},
    doi = {http://dx.doi.org/10.1016/0304-3975(91)90358-9},
    url = {http://www.sciencedirect.com/science/article/pii/0304397591903589},
    author = {Ricardo A. Baeza-Yates}
}

@article{dasg:3,
    title = {Directed acyclic subsequence graph -- Overview},
    journal = {Journal of Discrete Algorithms},
    volume = {1},
    number = {3-4},
    pages = {255--280},
    year = {2003},
    issn = {1570-8667},
    url = {http://www.sciencedirect.com/science/article/pii/S1570866703000297},
    author = {Maxime Crochemore and Bořivoj Melichar and Zdeněk Troníček},
    keywords = "Searching subsequences",
    keywords = "Directed acyclic subsequence graph",
    keywords = "Subsequence automation "
}

@INPROCEEDINGS{dasg:4,
    author = {Hoshino, H. and Shinohara, A. and Takeda, M. and Arikawa, S.},
    booktitle = {String Processing and Information Retrieval, 2000. SPIRE 2000. Proceedings. Seventh International Symposium on},
    title = {Online construction of subsequence automata for multiple texts},
    year = {2000},
    month = {},
    pages = {146--152},
    keywords = {computational complexity;deterministic automata;finite automata;query processing;set theory;text analysis;alphabet;deterministic finite automaton;minimum automaton;multiple texts;online algorithm;online construction;preprocessing;subsequence automata;subsequence automaton;Automata;Computational complexity;Data structures;Gain measurement;Informatics;Machine learning;Machine learning algorithms;Text recognition;Upper bound},
    doi = {10.1109/SPIRE.2000.878190}, }

@incollection{dasg:5,
    year = {2002},
    isbn = {978-3-540-44158-8},
    booktitle = {String Processing and Information Retrieval},
    volume = {2476},
    series = {Lecture Notes in Computer Science},
    editor = {Laender, AlbertoH.F. and Oliveira, ArlindoL.},
    title = {On the Size of DASG for Multiple Texts},
    url = {http://dx.doi.org/10.1007/3-540-45735-6_6},
    publisher = {Springer Berlin Heidelberg},
    author = {Crochemore, Maxime and Troníček, Zdeněk},
    pages = {58-64},
}

@misc{dataset:xmark,
    title = {XMark -- An XML Benchmark Project},
    author = {Schimdt{, et al}},
    url = {http://www.xml-benchmark.org/},
    howpublished = {online},
    citedate = {2015/04/27}
}

@misc{saxon,
    title = {SAXON -- The XSLT and XQuery Processor},
    author = {{SAXONICA}},
    url = {http://saxon.sourceforge.net/},
    howpublished = {online},
    citedate = {2015/04/28}
}

@misc{xmlgen,
    title = {xmlgen -- faq},
    author = {Florian Waas},
    url = {http://www.xml-benchmark.org/faq.txt},
    howpublished = {online},
    citedate = {2015/04/27}
}

@article{rabin1959finite,
    title = "Finite automata and their decision problems",
    author = "Michael O Rabin and Dana Scott",
    journal = {IBM journal of research and development},
    volume = {3},
    number = {2},
    pages = {114--125},
    year = {1959},
    publisher = {IBM}
}

@Online{Langerova,
  author       = {Jana Langerová},
  title        = {Projektové řízení: 10 nástrojů a zkušenosti z firem},
  url          = {https://www.cfoworld.cz/clanky/nastroje-pro-projektove-rizeni-jak-jsou-pro-nas-dnes-dulezite/},
  urldate      = {2022-04-24},
  journaltitle = {CFOworld},
  publisher    = {Internet Info DG, a.s.},
}

@Online{Lujan,
  author       = {Vince Lujan},
  title        = {What’s the Difference b/w SSO (Single Sign On) \& LDAP?},
  url          = {https://jumpcloud.com/blog/sso-vs-ldap},
  urldate      = {2022-04-24},
  journaltitle = {JumpCloud},
}

@MastersThesis{Michal2012,
  author   = {Michal,Knížek},
  date     = {2012},
  title    = {Finanční informační systém pro manažery},
  language = {Czech},
  abstract = {katedra počítačové grafiky a interakce},
}

@Online{caflou,
  author  = {{Macek Petr \& Co}},
  title   = {Caflou},
  url     = {https://www.caflou.cz/co-caflou-umi},
  urldate = {2022-04-24},
}

@Online{milestones,
  author  = {KIDASA Software, Inc.},
  title   = {What is Milestones Simplicity?},
  url     = {http://www.kidasasoftware.com/support/help/simp2019/What_is_Milestones_Simplicity.htm},
  urldate = {2022-05-02},
}

@Online{spirateam,
  author  = {{Inflectra Corporation}},
  title   = {SpiraTeam},
  url     = {https://www.inflectra.com/SpiraTeam/},
  urldate = {2022-05-02},
}

@Online{Aston,
  author       = {Ben Aston},
  title        = {What Is Microsoft Project Management Software? (Guide To MS Project \& MS Teams)},
  url          = {https://thedigitalprojectmanager.com/tools/microsoft-project-management-software-guide/},
  urldate      = {2022-05-02},
  journaltitle = {The Digital Project Manager},
}

@Book{Mikowski2013//,
  author    = {Michael Mikowski and Josh Powell},
  title     = {Single Page Web Applications: JavaScript End-To-end},
  isbn      = {9781638351344},
  location  = {New York, UNITED STATES},
  publisher = {Manning Publications Co. LLC},
  subtitle  = {JavaScript End-To-end},
  url       = {http://ebookcentral.proquest.com/lib/cvut/detail.action?docID=6642734},
  year      = {2013},
}

@Online{JohnI.Moore2022,
  author       = {Moore, John I., Jr.},
  date         = {2022},
  title        = {Why Kotlin? Eight features that could convince Java developers to switch},
  url          = {https://www.infoworld.com/article/3396141/why-kotlin-eight-features-that-could-convince-java-developers-to-switch.html},
  subtitle     = {What would Java look like if someone designed it from scratch today? Probably a lot like Kotlin},
  urldate      = {2022-05-03},
  journaltitle = {InfoWorld},
  publisher    = {IDG Communications, Inc.},
}

@Online{Springbootintro,
  author       = {Tutorialspoint},
  title        = {Spring Boot - Introduction},
  url          = {https://www.tutorialspoint.com/spring_boot/spring_boot_introduction.htm},
  urldate      = {2022-05-03},
  journaltitle = {Tutorialspoint},
}

@Online{gradle,
  author       = {{Gradle Inc.}},
  title        = {Gradle vs Maven Comparison},
  url          = {https://gradle.org/maven-vs-gradle/},
  urldate      = {2022-05-03},
  journaltitle = {Gradle Build Tool},
  publisher    = {Gradle Inc.},
}

@Online{Janik2019,
  author       = {David Janík},
  date         = {2019},
  title        = {Velké srovnání MySQL (MariaDB) a PostgreSQL},
  url          = {https://www.vas-hosting.cz/blog-velke-srovnani-mysql-mariadb-a-postgresql},
  urldate      = {2022-05-03},
  journaltitle = {Váš Hosting},
  publisher    = {Váš Hosting s.r.o.},
}

@Online{Pearce2022,
  author       = {Rohan Pearce},
  date         = {2022},
  title        = {Dead database walking: MySQL's creator on why the future belongs to MariaDB},
  url          = {https://www2.computerworld.com.au/article/457551/dead_database_walking_mysql_creator_why_future_belongs_mariadb/},
  subtitle     = {MySQL's creator, Michael "Monty" Widenius, is scathing on database's future with Oracle},
  urldate      = {2022-05-03},
  journaltitle = {Computerworld},
  publisher    = {IDG Communications, Inc.},
}

@Online{2021,
  author       = {Sacha Greif and Raphaël Benitte},
  date         = {2021},
  title        = {JavaScript Flavors},
  url          = {https://2020.stateofjs.com/en-US/technologies/javascript-flavors/},
  urldate      = {2022-05-03},
  journaltitle = {State of JS 2020},
}

@Online{Facebook,
  author       = {Facebook},
  title        = {Flux},
  url          = {https://github.com/facebook/flux},
  urldate      = {2022-05-04},
  journaltitle = {Github},
}

@Online{Rodriguez,
  author       = {Rubén Rodríguez},
  title        = {Pinia},
  url          = {https://rubenr.dev/en/pinia-vuex/},
  subtitle     = {The friendlier alternative to Vuex in Vue 3},
  urldate      = {2022-05-04},
  journaltitle = {Rubenr.dev},
}

@Online{Pattakos,
  author       = {Aris Pattakos},
  title        = {14 Best Vue UI Component Libraries 2022},
  url          = {https://athemes.com/collections/vue-ui-component-libraries/},
  urldate      = {2022-05-04},
  journaltitle = {AThemes},
}

@Online{Chandrakant,
  author       = {Kumar Chandrakant},
  title        = {Concurrency in Spring WebFlux},
  url          = {https://www.baeldung.com/spring-webflux-concurrency},
  urldate      = {2022-05-09},
  journaltitle = {Baeldung},
}

@Online{kotest,
  author       = {{Kotest Team}},
  title        = {Testing styles},
  url          = {https://kotest.io/docs/5.2/framework/testing-styles.html},
  urldate      = {2022-05-09},
  journaltitle = {Kotest},
}

@Online{vueApiStyles,
  title        = {Introduction},
  url          = {https://vuejs.org/guide/introduction.html#api-styles},
  subtitle     = {API styles},
  urldate      = {2022-05-10},
  journaltitle = {Vue.js},
}

@Online{vuekeycloak,
  author       = {Direktoratet for samfunnssikkerhet og beredskap},
  title        = {Vue Keycloak Plugin},
  url          = {https://github.com/dsb-norge/vue-keycloak-js},
  urldate      = {2022-05-10},
  journaltitle = {Github},
}

@Online{Richardson,
  author       = {Leonard Richardson},
  title        = {Justice Will Take Us Millions Of Intricate Moves},
  url          = {https://www.crummy.com/writing/speaking/2008-QCon/act3.html},
  subtitle     = {Act Three: The Maturity Heuristic},
  urldate      = {2022-05-10},
  journaltitle = {Crummy},
}

@Online{Messner,
  author       = {Gragory M. Messner},
  title        = {GitlLab4J™ API},
  url          = {http://www.messners.com/\#gitlab4j-api/gitlab4j-api.html},
  subtitle     = {Java Client Library for the GitLab